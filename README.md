# heroku-buildpack-mariadb

[![Build Status](https://secure.travis-ci.org/Shopify/heroku-buildpack-mysql.png)](http://travis-ci.org/Shopify/heroku-buildpack-mysql)

This is a [Heroku buildpack](http://devcenter.heroku.com/articles/buildpacks) for vendoring the MariaDB mysql client binaries into your project.

## Versions

* MariaDB: `10.3`

## Bug Notice

This will only copy over `mysqldump` as part of the buildpack for now.
